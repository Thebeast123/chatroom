 Template.home.helpers({
     "posts": function () {
         return Posts.find({}).fetch();
     }
 });

 Template.friends.helpers({
     listUser: function () {
         return Meteor.users.find({
             _id: {
                 $ne: Meteor.userId()
             }
         }, {
             sort: {
                 'profile.firstname': 1
             }
         });
     }
 });