Meteor.publish('posts', function () {
    if (this.user.Id) {
        return Posts.find();
    }
});

Meteor.startup(function () {
    ChatRooms.allow({
        'insert': function (userId, doc) {
            return true;
        },
        'update': function (userId, doc, fieldNames, modifier) {
            return true;
        },
        'remove': function (userId, doc) {
            return false;
        }
    });
});
Meteor.publish("chatrooms", function () {
    return ChatRooms.find({});
});
Meteor.publish("onlusers", function () {
    return Meteor.users.find({
        "status.online": true
    }, {
        username: 1
    });
});