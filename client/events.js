Template.home.events({
    "submit .name-form": function (event) {
        event.preventDefault();
        var name = event.target.firstname.value;

        Posts.insert({
            name: name,
            username: Meteor.user().username,
            createdAt: new Date()
                //group:
        });
    }
});

Template.home.events({
    'click .delete-post': function (event) {
        Posts.remove({
            _id: this._id
        });
    }
});